"""
Demonstration of the GazeTracking library.
Check the README.md for complete documentation.
"""

import cv2, time, datetime, math
import numpy as np
from gaze_tracking import GazeTracking

gaze = GazeTracking()
webcam = cv2.VideoCapture(0)
display_generate = True
display = None

height = 640
width = 800
display = np.zeros((height,width,3), np.uint8)
layer = np.zeros((height,width,3), np.uint8)

# B G R A
display[:,:,0] = 0
display[:,:,1] = 0
display[:,:,2] = 0

halo = cv2.imread("halo.png");

def str_float( f ):
	return str(f).replace('.',',')

def draw_eye( image, eye, pupil ):
	
	if eye == None:
		return
	
	eye_origin = eye.origin
	eye_center = eye.center
	eye_size = ( eye.center[0]*2, eye.center[1]*2 )
	eye_radius = eye.center[0]
	if eye_radius < eye.center[1]:
		eye_radius = eye.center[1]
	
	#        B  G  R
	color = (0,255,255)
	cv2.rectangle( image, (int(eye_origin[0]),int(eye_origin[1])), (int(eye_origin[0]+eye_size[0]),int(eye_origin[1]+eye_size[1])), color, 1 )
	color = (255,255,0)
	cv2.circle( image, (int(eye_origin[0]+eye_center[0]),int(eye_origin[1]+eye_center[1])), int(eye_radius), color, 1 )
	
	if pupil != None:
		
		# there is a pupil seen!!
		color = (0, 255, 0)
		cv2.line(image, (pupil[0] - 5, pupil[1]), (pupil[0] + 5, pupil[1]), color)
		cv2.line(image, (pupil[0], pupil[1] - 5), (pupil[0], pupil[1] + 5), color)
		color = (0,0,255)
		cv2.line(image, (pupil[0], pupil[1]), (int(eye_origin[0]+eye_center[0]),int(eye_origin[1]+eye_center[1])), color)
		
		# calcul d'angles
		# angle X
		A = eye_radius
		O = pupil[0] - (eye_origin[0]+eye_center[0])
		angle_alpha = math.atan( O/A )
		# print( "horizontal: " + str( angle_alpha / math.pi * 180 ) )
		# angle Y
		A = eye_radius
		O = pupil[1] - (eye_origin[1]+eye_center[1])
		angle_beta = math.atan( O/A )
		# #####print( "vertical: " + str( angle_beta / math.pi * 180 ) )
		
	return

def pupil_move(image, x, y, minx, maxx, miny, maxy):
	coord_x = x - minx
	coord_y = y - miny
	
	coord_x *= width/(maxx - minx)
	coord_y *= height/(maxy- miny)
	print("Scaled horiz: ", coord_x, ", scaled vert: ", coord_y)
	
	color = (0, 0, 255)
	
	layer[:,:,0] = 0
	layer[:,:,1] = 0
	layer[:,:,2] = 0
	
	#cv2.circle(layer, (int(coord_x), int(coord_y)), 20, color, -1)
	layer[int(coord_x):int(coord_x)+100,int(coord_y):int(coord_y)+100,2] = halo[:,:,2]
	
	return cv2.addWeighted(image, 0.98, layer, 0.2, 0.0); 

f = open( "data.csv", 'w' )
sum_g = [0, 0]
sum_d = [0, 0]
mean_g = [0, 0]
mean_d = [0, 0]
nb = 0
begin = datetime.datetime.now()

	#display = np.full((height,width,3),255,np.uint8)
	#display_generate = False


while True:
	
	# We get a new frame from the webcam
	_, frame = webcam.read()

	#if display_generate:
	#height, width, channels = frame.shape

	# We send this frame to GazeTracking to analyze it
	gaze.refresh(frame)

	#frame = gaze.annotated_frame()
	text = ""

	'''
	if gaze.is_blinking():
		text = "Blinking"
	elif gaze.is_right():
		text = "Looking right"
	elif gaze.is_left():
		text = "Looking left"
	elif gaze.is_center():
		text = "Looking center"
	'''
	
	txt_color = (0,0,255)
	cv2.putText(frame, text, (20, 30), cv2.FONT_HERSHEY_DUPLEX, 0.5, txt_color, 0)

	left_pupil = gaze.pupil_left_coords()
	right_pupil = gaze.pupil_right_coords()
	cv2.putText(frame, "Left pupil:  " + str(left_pupil), (20, 50), cv2.FONT_HERSHEY_DUPLEX, 0.5, txt_color, 0)
	cv2.putText(frame, "Right pupil: " + str(right_pupil), (20, 70), cv2.FONT_HERSHEY_DUPLEX, 0.5, txt_color, 0)
	
	#ratios
	cv2.putText(frame, "horizontal:  " + str(gaze.horizontal_ratio()), (20, 90), cv2.FONT_HERSHEY_DUPLEX, 0.5, txt_color, 1)
	cv2.putText(frame, "vertical:  " + str(gaze.vertical_ratio()), (20, 110), cv2.FONT_HERSHEY_DUPLEX, 0.5, txt_color, 1)
	
	draw_eye( frame, gaze.eye_left, gaze.pupil_left_coords() )
	draw_eye( frame, gaze.eye_right, gaze.pupil_right_coords() )
	
	#draw_eye( display, gaze.eye_left, gaze.pupil_left_coords() )
	#draw_eye( display, gaze.eye_right, gaze.pupil_right_coords() )
	
	if left_pupil != None and right_pupil != None:
		nb += 1
		for i in [0,1]:
			sum_g[i] += gaze.eye_left.center[i]
			sum_d[i] += gaze.eye_right.center[i]
			mean_g[i] = sum_g[i]/nb
			mean_d[i] = sum_d[i]/nb
		display = pupil_move(display, left_pupil[0] - gaze.eye_left.origin[0] - gaze.eye_left.center[0], left_pupil[1] - gaze.eye_left.origin[1] - mean_g[1], -<26, 16, -10, 5)

		line = str(datetime.datetime.now() - begin) + ';' + str_float(gaze.eye_left.origin[0]) + ';' + str_float(gaze.eye_left.origin[1])  + ';' + str_float(gaze.eye_right.origin[0]) + ';' + str_float(gaze.eye_right.origin[1]) + ';' + str_float(gaze.eye_left.center[0])  + ';' + str_float(gaze.eye_left.center[1]) + ' ; ' + str_float(gaze.eye_right.center[0]) + ' ; ' + str_float(gaze.eye_right.center[1]) + ';' + str_float(left_pupil[0] - gaze.eye_left.origin[0] - gaze.eye_left.center[0]) + ';' + str_float(left_pupil[1] - gaze.eye_left.origin[1] - mean_g[1])  + ';' + str_float(right_pupil[0] - gaze.eye_right.origin[0] - gaze.eye_left.center[1]) + ';' + str_float(right_pupil[1] - gaze.eye_right.origin[1] - mean_g[1]) 

		f.write( line + '\n' )
	
	display = cv2.GaussianBlur(display, (5, 5), 0) 
	
	cv2.imshow("Demo", frame)
	cv2.imshow("Display", display)
	cv2.imshow("Layer", layer)

	if cv2.waitKey(1) == 27:
		f.close()
		break
