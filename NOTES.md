https://github.com/antoinelame/GazeTracking

# pour installer

```
git clone https://github.com/antoinelame/GazeTracking.git
cd GazeTracking
sudo apt install python3-dev
sudo pip3 install -r requirements.txt
sudo pip3 install --upgrade numpy
```

(version of numpy required: min 1.17)

sohcahtoa

```
   |\
   | \
 O |  \ H
   |   \
   |__α(\
      
      A
```

O = opposite
H = hypothenuse
A = adjacent

relations:

sin α = O/H
cos α = A/H
tan α = O/A
