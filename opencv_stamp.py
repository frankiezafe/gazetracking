import cv2, time, math
import numpy as np

frame_size = (600,800)
stamp_size = (60,60) # toujours PAIR!!!

def generate_halo( im, r, g, b ):
	
	width = im.shape[1]
	height = im.shape[0]
	hw = width * 0.5
	hh = height * 0.5
	
	for y in range( 0, height ):
		for x in range( 0, width ):
			dx = ( x - hw ) / hw
			dy = ( y - hh ) / hh
			d = max(0, 1 - math.sqrt( dx * dx + dy * dy ) )
			im[x,y,0] = int(b*d)
			im[x,y,1] = int(g*d)
			im[x,y,2] = int(r*d)

def print_image( im, stamp, x, y, alpha_im, alpha_stamp ):
	
	layer[:,:,0] = 0
	layer[:,:,1] = 0
	layer[:,:,2] = 0
	
	startim = [ int( y - stamp.shape[0] / 2 ), int( x - stamp.shape[1] / 2 ) ]
	endim = [ int( y + stamp.shape[0] / 2 ), int( x + stamp.shape[1] / 2 ) ]
	
	startstamp = [ 0, 0 ]
	endstamp = [ stamp.shape[0], stamp.shape[1] ]
	
	for i in range(0,2):
		if startim[i] < 0:
			startstamp[i] -= startim[i]
			startim[i] = 0
		if endim[i] > im.shape[i]:
			endstamp[i] -= endim[i] - im.shape[i]
			endim[i] = im.shape[i]
	
	print( startim, endim, startstamp, endstamp )
	
	# aucun pixel du stamp n'est visible
	if endim[0] < 0 or endim[1] < 0:
		return im
	if startstamp[0] < 0 or startstamp[1] < 0:
		return im
	if endstamp[0] < 0 or endstamp[1] < 0:
		return im
	
	layer[ startim[0] : endim[0],  startim[1] : endim[1] ] = stamp[ startstamp[0] : endstamp[0],  startstamp[1] : endstamp[1] ]
	
	return cv2.addWeighted( im, alpha_im, layer, alpha_stamp, 0.0 )

frame = np.zeros((frame_size[0],frame_size[1],3), np.uint8)
layer = np.zeros((frame_size[0],frame_size[1],3), np.uint8)

stamp0 = np.zeros((stamp_size[0],stamp_size[1],3), np.uint8)
stamp1 = np.zeros((stamp_size[0],stamp_size[1],3), np.uint8)
generate_halo( stamp0, 255, 255, 0 )
generate_halo( stamp1, 0, 255, 255 )

#frame = print_image( frame, stamp0, 100, 100, 0.9, 0.5 )
#frame = print_image( frame, stamp0, 110, 200, 0.9, 0.5 )
#frame = print_image( frame, stamp0, 120, 300, 0.9, 0.5 )
#frame = print_image( frame, stamp0, 130, 400, 0.9, 0.5 )

pos = [ 0,0 ]
dir = [ 1.0,1.0 ]

while True:
	
	time.sleep( 1.0/10 )
	
	frame = cv2.GaussianBlur( frame, (15,15), 0 )
	
	frame = print_image( frame, stamp0, pos[1], pos[0], 0.9, 0.5 )
	
	for i in range(0,2):
		pos[i] += dir[i] * 20
		if pos[i] < 0 or pos[i] > frame.shape[i]:
			dir[i] *= -1
	
	#frame = print_image( frame, stamp1, x, 300 + y, 0.9, 0.5 )

	cv2.imshow( "frame", frame )
	cv2.imshow( "layer", layer )
	#cv2.imshow( "stamp0", stamp0 )
	#cv2.imshow( "stamp1", stamp1 )
	
	if cv2.waitKey(1) == 27:
		break

